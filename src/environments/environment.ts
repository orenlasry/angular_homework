// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBv2z1ZcKgJRWS6KtsIMhunQSwxACzpY64",
    authDomain: "hello-jce-lasry.firebaseapp.com",
    databaseURL: "https://hello-jce-lasry.firebaseio.com",
    projectId: "hello-jce-lasry",
    storageBucket: "hello-jce-lasry.appspot.com",
    messagingSenderId: "160320637764",
    appId: "1:160320637764:web:23137b2f478ad728402511"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
