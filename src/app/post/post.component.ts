import { Posts } from './../interfaces/posts';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { PostsService } from './../posts.service';

@Component({
  selector: 'post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  title:string;
  body:string;
  id:number;
  Posts$:Observable<Posts>;


  constructor(private PostsService:PostsService) { }

  ngOnInit(): void {
    this.Posts$ =  this.PostsService.PostsData();

      }


}
