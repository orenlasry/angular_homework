import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  books = [{title:'Alice in Wonderland',author:'Lewis Carrol', summary:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley'},
        {title:'War and Peace',author:'Leo Tolstoy', summary:'of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged'},
        {title:'The Magic Mountain', author:'Thomas Mann', summary:'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum'}];

  // public addBooks(){
  //   setInterval(()=>this.books.push({title:'A new one',author:'New author',summary:'Short summary'}),2000);
  // }


bookCollection:AngularFirestoreCollection;
userCollection:AngularFirestoreCollection = this.db.collection('users');

public getBooks(userId, startAfter){
this.bookCollection = this.db.collection(`users/${userId}/books`,
ref => ref.orderBy('title', 'asc').limit(4).startAfter(startAfter));
return this.bookCollection.snapshotChanges()
}
public getBooks2(userId, endBefore){
  this.bookCollection = this.db.collection(`users/${userId}/books`,
  ref => ref.orderBy('title', 'asc').limitToLast(4).endBefore(endBefore));
  return this.bookCollection.snapshotChanges()
  }

/*
public getBooks(userId){
  this.bookCollection = this.db.collection(`users/${userId}/books`);
  return this.bookCollection.snapshotChanges().pipe(map(
    collection =>collection.map(
      document => {
      const data = document.payload.doc.data();
      data.id = document.payload.doc.id;
      return data;
      }
    )
  ))
  }
  */
deleteBook(Userid:string, id:string){
  this.db.doc(`users/${Userid}/books/${id}`).delete();
}

updateBook(userId:string, id:string, title:string, author:string){
  this.db.doc(`users/${userId}/books/${id}`).update(
    {
      title:title,
      author:author
    }
  )
}

addBook(userId:string, title:string, author:string){
  const book = {title:title, author:author};
  this.userCollection.doc(userId).collection('books').add(book)
}

  constructor(private db:AngularFirestore) { }
     //   public getBooks(){
    //   const bookObservable = new Observable(obs => {
    //     setInterval(()=>obs.next(this.books),500)
    //   });
    //   return bookObservable;
    // }

  // public getBooks(){
  //   return this.books;
  // }
}
