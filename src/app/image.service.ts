import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  private path:string = 'https://firebasestorage.googleapis.com/v0/b/hello-jce-lasry.appspot.com/o/';

  public images:string[] = [];

  constructor() {
    this.images [0] = this.path + 'biz.JPG' + '?alt=media&token=e99c127d-9c83-477d-ac15-625e35925ea3';
    this.images [1] = this.path +'entermnt.JPG' + '?alt=media&token=b78c289e-3c81-4690-b885-84ee239880e5';
    this.images [2] = this.path + 'politics-icon.png' + '?alt=media&token=ea9c4c1e-59e4-4ad0-a16c-bc6b29663758';
    this.images [3] = this.path + 'sport.JPG' + '?alt=media&token=fcd64983-7f13-48c7-8df0-7d8d3623a0a8';
    this.images [4] = this.path +'tech.JPG' + '?alt=media&token=e5835e5a-1d0a-413e-a26c-1688552bdf5b';
   }
}
