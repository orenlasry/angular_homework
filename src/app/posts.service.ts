import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Posts} from './interfaces/posts';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private URL = "https://jsonplaceholder.typicode.com/posts"

  constructor(private http:HttpClient) { }
  PostsData():Observable<Posts>{
    return this.http.get<Posts>(`${this.URL}`)

  }



  private transformPostData(data:Posts):Posts{
    return{
     id:data.id,
     title:data.title,
     body:data.body

    }
  }

}
